var isMini = window.mini;
var STRING = (isMini ? "B P" : "B A R P R1 O J E C T").split(" ");
var BASES = [
  "Brandy",
  "Cachaca",
  "Gin",
  "Grappa",
  "Mezcal",
  "Pisco",
  "Sake",
  "Rum",
  "Tequila",
  "Vodka",
  "Whysky",
  "Nettari",
  "Soda",
  "Ghiaccio",
];
var GLASSES = ["shot", "rock", "cocktail", "wine", "flute", "tumbler"];
var FLAVOURS = [];
var SCALES = ["piccola", "media", "grande"];

var letterPaths = {};
var bannerPaths = {};
var patternPaths = {};
var patterns;

var stage;
var letterContainer;

var currentBases = [0];
var currentFlavours = [];
var currentLetters = [];
var currentPatternContainer;
var currentBanner;

var basesSettings = {};
var flavourSettings = {};

var gui;
var f1;
var f2;
var f3;
var f4;

var settings = {};

$(function () {
  var w = $(window).innerWidth();
  var h = $(window).innerHeight();

  stage = SVG("drawing").size(w - 20, h - 20);
  letterContainer = stage.nested();

  load((isMini ? "small-" : "") + "letters" + ".svg", onLettersLoaded);
});

function load(assetsURL, callback) {
  $.ajax({
    url: "assets/" + assetsURL,
  })
    .done(callback)
    .fail(
      function () {
        console.log("error");
      }.bind(this)
    );
}

// ------------- loading callbacks

function onLettersLoaded(a) {
  var letters = $("svg > g", a);
  for (var i = 0; i < letters.length; i++) {
    addToList($(letters[i]));
  }

  function addToList($el) {
    // trim crap added by illustrator
    var name = $el.attr("id");
    var regexpatterns = ["__x2013__", "_-_"];
    var found = false;

    for (var i = 0; i < regexpatterns.length; i++) {
      var pattern = regexpatterns[i];

      if (name.match(pattern)) {
        var a = name.split(pattern);
        var letterName = a[0];
        var baseName = a[1];

        if (baseName.match("_1_")) {
          baseName = baseName.split("_1_")[0];
        }

        // check for errors
        if (STRING.indexOf(letterName) == -1)
          console.error("Layer ", name, " is not named properly");
        if (BASES.indexOf(baseName) == -1)
          console.error("Layer ", name, " is not named properly");

        if (!letterPaths[letterName]) {
          letterPaths[letterName] = {};
        }

        if (!letterPaths[letterName][baseName]) {
          letterPaths[letterName][baseName] = {};
        }
        letterPaths[letterName][baseName].path = $el;
        found = true;
        break;
      }
    }

    if (!found) console.error("Couldn't find letter - has the svg changed?");
  }

  load((isMini ? "small-" : "") + "banners.svg", onBannersLoaded);
}

function onBannersLoaded(a) {
  var banners = $("svg > g", a);

  for (var i = 0; i < banners.length; i++) {
    var id = banners[i].getAttribute("id");
    bannerPaths[id] = banners[i];
  }

  load("patterns.svg", onPatternsLoaded);
}

function onPatternsLoaded(a) {
  patterns = $("svg > g", a);

  for (var i = 0; i < patterns.length; i++) {
    var id = patterns[i].getAttribute("id");
    FLAVOURS.push(id);
  }

  init();
}

// ------------- init

function init() {
  $("#loading").hide();

  currentBanner = stage.nested();
  currentPatternContainer = stage.nested();
  currentBannerStroke = stage.nested();

  currentBanner.viewbox({
    x: 0,
    y: 0,
    width: 2829,
    height: 473,
  });

  currentBannerStroke.viewbox({
    x: 0,
    y: 0,
    width: 2829,
    height: 473,
  });

  currentPatternContainer.viewbox({
    x: 0,
    y: 0,
    width: 2829,
    height: 473,
  });

  window.onresize = resize;

  initSettings();
  initUI();
  parseSettingsHash();
  generatePatterns();
  generate();
}

function initSettings() {
  settings.negativeMode = false;
  settings.tinta = 100;
  settings.bicchiere = GLASSES[0];
  settings.intensita = 10;
  settings.seed = (Math.round(Math.random() * 100) / 100).toString();
  settings.secondDistance = 25;
  settings.saturation = 90;
  settings.luminosity = 75;
  settings.noBg = false;
  settings.scalePattern = SCALES[SCALES.length - 1];

  for (var i = 0; i < BASES.length; i++) {
    basesSettings[BASES[i]] = false;
  }

  console.log(FLAVOURS.join('", "'));

  for (var i = 0; i < FLAVOURS.length; i++) {
    flavourSettings[FLAVOURS[i]] = false;
  }

  //set Ghiaccio and one flavour
  basesSettings[BASES[13]] = true;

  settings.miscela = miscela.bind(this);
  settings.rigenera = rigenera.bind(this);
  settings.esporta = esporta.bind(this);
}

function initUI() {
  gui = new dat.GUI();
  console.log(gui);
  if (!isMini) {
    f3 = gui.addFolder("Bicchiere");
    f1 = gui.addFolder("Basi");
    f2 = gui.addFolder("Gusti");
    f4 = gui.addFolder("Altro");

    for (var i = 0; i < BASES.length; i++) {
      var d1 = f1.add(basesSettings, BASES[i]).listen();
      d1.onChange(function (a) {
        var same = this.object[this.property] == a;
        if (!same) return;

        var isChecked = this.__checkbox.checked;

        if (isChecked && currentBases.length >= 5) {
          window.alert(
            "Limite basi raggiunto. Deseleziona delle basi per aggiungerne altre."
          );
          this.object[this.property] = false;
          this.updateDisplay();
        } else if (!isChecked && currentBases.length == 1) {
          window.alert("E' necessaria almeno una base.");
          this.object[this.property] = true;
          this.updateDisplay();
        } else {
          createLetters();
          changeLettersColours();
          updateSettingsHash();
          resize();
        }
      });
    }

    for (var i = 0; i < FLAVOURS.length; i++) {
      var d1 = f2.add(flavourSettings, FLAVOURS[i]).listen();

      d1.onChange(function (a, b) {
        var isChecked = this.__checkbox.checked;

        if (isChecked && currentFlavours.length >= 2) {
          window.alert(
            "Limite gusti raggiunto. Deseleziona dei gusti per aggiungerne altri."
          );
          this.object[this.property] = false;
          this.updateDisplay();
        }

        changePatterns();
        changeColour();
        changeIntensity();
        updateSettingsHash();
        resize();
      });
    }

    var cIntensita = f2.add(settings, "intensita", 0, 20).step(1).listen();

    cIntensita.onChange(function () {
      changeIntensity();
      updateSettingsHash();
      resize();
    });

    var cBicchiere = f3.add(settings, "bicchiere", GLASSES).name("tipo");

    var noBgController = f3.add(settings, "noBg").name("colorato").listen();

    noBgController.onChange(function (a) {
      updateColourValue();
      changeColour();
      updateSettingsHash();
    });

    var cTinta = f3.add(settings, "tinta", 0, 360).name("colore").listen();

    settings.controllerTinta = cTinta;

    cTinta.onChange(function () {
      updateColourValue();
      changeColour();
      updateSettingsHash();
    });

    cBicchiere.onChange(function () {
      createBanner();
      changeColour();
      updateSettingsHash();
      resize();
    });

    f4.add(settings, "negativeMode", true)
      .name("negativo")
      .listen()
      .onChange(function () {
        changeColour();
        updateSettingsHash();
      });

    f4.add(settings, "scalePattern", SCALES)
      .name("scala pattern")
      .onChange(function () {
        changePatterns();
        changeIntensity();
        changeColour();
        updateSettingsHash();
      });

    gui.add(settings, "miscela").name("Miscela");
  }

  gui.add(settings, "rigenera").name("Random");
  gui.add(settings, "esporta").name("Esporta");
}

// ------------- hash

function parseSettingsHash() {
  var currentState = window.location.hash.split("#");

  if (currentState.length > 1) {
    var a = currentState[1].split("&");

    var obj = {};
    for (var i = 0; i < a.length; i++) {
      var params = a[i].split("=");
      obj[params[0]] = params[1];
    }

    var bases = obj["base"].split(",");
    for (var i in basesSettings) {
      basesSettings[i] = false;
    }
    for (var i = 0; i < bases.length; i++) {
      basesSettings[BASES[bases[i]]] = true;
    }

    if (obj["gusto"]) {
      var flavours = obj["gusto"].split(",");
      for (var i = 0; i < flavourSettings.length; i++) {
        flavourSettings[i] = false;
      }
      for (var i = 0; i < flavours.length; i++) {
        flavourSettings[FLAVOURS[flavours[i]]] = true;
      }
    }

    console.log(flavourSettings);

    settings["bicchiere"] = GLASSES[obj["bicchiere"]];
    settings["intensita"] = parseInt(obj["intensita"]);
    settings["negativeMode"] = obj["negativeMode"] === "true";
    settings["noBg"] = obj["noBg"] === "true";
    settings["scalePattern"] = obj["scalePattern"];
    settings["seed"] = parseFloat(obj["seed"]);
    settings["tinta"] = parseInt(obj["tinta"]);
  }
}

function updateSettingsHash() {
  var s = "";
  s += "base=";
  for (var i = 0; i < currentBases.length; i++) {
    s += BASES.indexOf(currentBases[i]) + ",";
  }
  s = s.substring(0, s.length - 1);

  if (currentFlavours.length) {
    s += "&";
    s += "gusto=";
    for (var i = 0; i < currentFlavours.length; i++) {
      s += FLAVOURS.indexOf(currentFlavours[i]) + ",";
    }
    s = s.substring(0, s.length - 1);
  }

  s += "&";
  s += "bicchiere=" + GLASSES.indexOf(settings.bicchiere);
  s += "&";
  s += "tinta=" + Math.round(settings.tinta);
  s += "&";
  s += "intensita=" + Math.round(settings.intensita);
  s += "&";
  s += "scalePattern=" + settings.scalePattern;
  s += "&";
  s += "negativeMode=" + settings.negativeMode;
  s += "&";
  s += "noBg=" + settings.noBg;
  s += "&";
  s += "seed=" + settings.seed;

  location.hash = (null, null, "#" + s);
}

// ------------- generation

function generate() {
  updateColourValue();
  changePatterns();
  createBanner();
  createLetters();
  changeColour();
  changeIntensity();
  resize();
}

function generatePatterns() {
  // generate patterns for each scale to save time and calculations
  var scales = [0.5, 0.65, 1];

  for (var ii = 0; ii < scales.length; ii++) {
    var scale = scales[ii];

    for (var i = 0; i < patterns.length; i++) {
      var id = patterns[i].getAttribute("id");
      var patternContainer = stage.nested();
      var patternPath = patterns[i].innerHTML.toString();

      if (!patternPaths[id]) patternPaths[id] = [];
      patternPaths[id][ii] = patternContainer;

      var w = 0;
      var h = 0;

      for (var x = 0; x < 100; x++) {
        for (var y = -1; y < 50; y++) {
          if (y * h * scale > 473) break;

          var nested = patternContainer.nested();
          var g = nested.svg(patternPath);

          if (w === 0) {
            w = nested.bbox().width;
            h = nested.bbox().height;
          }

          nested.viewbox({
            x: 0,
            y: 0,
            width: w,
            height: h,
          });

          nested.width(w * scale);
          nested.height(h * scale);

          nested.x((x * w - w * 0.5) * scale);
          nested.y((y * h - h * 0.5) * scale);
        }

        if (x * w * scale > 2829) break;
      }

      patternContainer.style("display", "none");
    }
  }
  $("#loading").hide();
}

function changePatterns() {
  if (!flavourSettings) flavourSettings = {};
  var currentScaleIndex = SCALES.indexOf(settings.scalePattern);

  //reset pattern container
  currentPatternContainer.clear();
  currentFlavours = [];

  for (var i in flavourSettings) {
    if (flavourSettings[i] == true) {
      currentFlavours.push(i);
      currentPatternContainer.add(
        patternPaths[i][currentScaleIndex].style("display", "block")
      );
    }
  }
}

function createLetters() {
  currentBases = [];

  for (var i in basesSettings) {
    if (basesSettings[i]) {
      currentBases.push(i);
    }
  }

  // assign bases to letters
  var a = [];
  for (var i = 0; i < STRING.length; i++) {
    a.push(i % currentBases.length);
  }

  shuffle(a);

  //remove current letters
  for (var i = 0; i < currentLetters.length; i++) {
    currentLetters[i].remove();
  }

  currentLetters = [];

  for (var i = 0; i < STRING.length; i++) {
    var letterName = STRING[i];
    var p = letterPaths[letterName][currentBases[a[i]]].path;

    var group = stage.nested();
    group.svg(p[0].innerHTML.toString());
    group.viewbox({
      x: 0,
      y: 0,
      width: 347.386,
      height: 364.688,
    });

    currentLetters.push(group);
  }
}

function createBanner() {
  currentBanner.clear();
  currentBanner.back();
  currentBannerStroke.clear();

  var p = bannerPaths[settings.bicchiere].innerHTML.toString();
  currentBanner.svg(p);
  currentBannerStroke.svg(p);

  var path = currentBanner.first().first();
  path.style("stroke", "none");
  path.style("fill", settings.hueHSB);

  var d = $("path", p);

  if (!d.length) {
    d = $("polygon", p);
    currentPatternContainer.clipWith(
      currentPatternContainer.polygon(d.attr("points")).fill("#000")
    );
  } else {
    currentPatternContainer.clipWith(
      currentPatternContainer.path(d.attr("d")).fill("#000")
    );
  }
}

// ------------- modifications

function changeIntensity() {
  var children = currentPatternContainer.children();

  for (var z = 0; z < children.length; z++) {
    var subchildren = children[z].children();

    for (var i = 0; i < subchildren.length; i++) {
      var c = subchildren[i].first().children();

      for (var n = 0; n < c.length; n++) {
        if (c[n].attr("stroke") != "#FF0000") {
          c[n].attr("stroke-width", settings.intensita);
        } else {
          c[n].style("display", "none");
        }
      }
    }
  }
}

function changeColour() {
  changeLettersColours();

  var bannerPath = currentBanner.first().first();

  if (settings.negativeMode) {
    bannerPath.style("fill", "#000");

    currentBannerStroke.select("path").style("stroke", "#fff");
    currentBannerStroke.select("polygon").style("stroke", "#fff");
    currentPatternContainer.style("display", "none");

    if (settings.negativeMode) {
      document.body.className = "negative";
    }
  } else {
    bannerPath.style("fill", settings.hueHSB);
    currentBannerStroke.select("path").style("stroke", "#000");
    currentBannerStroke.select("polygon").style("stroke", "#000");

    document.body.className = "positive";

    if (!settings.noBg) {
      currentPatternContainer.style("display", "none");
      bannerPath.style("fill", "#fff");
      return;
    } else {
      currentPatternContainer.style("display", "block");
    }

    var children = currentPatternContainer.children();
    var availableStrokes = ["compHueHSB", "secondHueHSB"];

    for (var z = 0; z < children.length; z++) {
      var subchildren = children[z].children();
      for (var i = 0; i < subchildren.length; i++) {
        var c = subchildren[i].first().children();
        for (var n = 0; n < c.length; n++) {
          if (c[n].attr("stroke") != "#FF0000") {
            c[n].style({
              stroke: settings[availableStrokes[z]],
            });
          }
        }
      }
    }
  }
}

function changeLettersColours() {
  var colour = settings.negativeMode ? "#fff" : "#000000";
  for (var i = 0; i < currentLetters.length; i++) {
    currentLetters[i].style("fill", colour);
    currentLetters[i].select("path").attr("stroke", colour);
  }
}

function updateColourValue() {
  if (settings.noBg) {
    var hue = settings.tinta;
    var comphue = (hue + 100) % 360;
    settings.hueHSB = getHSLString(hue);
    settings.compHueHSB = getHSLString(comphue);
    settings.secondHueHSB = getHSLString(
      (comphue + settings.secondDistance) % 360
    );
    if (settings.controllerTinta)
      settings.controllerTinta.__foreground.style.background = settings.hueHSB;
  }

  $(settings.controllerTinta && settings.controllerTinta.domElement)
    .parent()
    .attr("disabled", !settings.noBg);
}

function rigenera() {
  settings.noBg = Math.random() < 0.8 ? true : false;
  settings.tinta = Math.round(Math.random() * 360);
  settings.bicchiere =
    GLASSES[Math.round(Math.random() * (GLASSES.length - 1))];
  settings.intensita = Math.random() * 18 + 3;
  settings.scalePattern =
    SCALES[Math.round(Math.random() * (SCALES.length - 1))];

  var randomBasesN = Math.round(Math.random() * 3) + 1;
  var c = 0;
  for (var i in basesSettings) {
    if (Math.random() > 0.5 && c < randomBasesN) {
      basesSettings[i] = true;
      c++;
    } else {
      basesSettings[i] = false;
      currentBases;
    }
  }

  var randomFlavoursN = Math.round(Math.random()) + 1;
  c = 0;
  for (var i in flavourSettings) {
    if (Math.random() > 0.5 && c < randomFlavoursN) {
      flavourSettings[i] = true;
      c++;
    } else {
      flavourSettings[i] = false;
    }
  }

  generate();

  updateSettingsHash();
}

function resize() {
  var w = $(window).innerWidth() - 300;
  var h = $(window).innerHeight();
  stage.size(w, h - 20);

  var padding = w * 0.2;
  var z = w - padding * 2;

  var k = z / 10;
  var n = k * 0.9;
  var ratio = 364.688 / 347.386;

  var startX = isMini ? (w - n * 2) * 0.5 : (w - z) * 0.5;

  // position letters
  for (var i = 0; i < STRING.length; i++) {
    var group = currentLetters[i];
    group.width(n);
    group.height(n * ratio);
    group.x(i * k + startX);
    group.y((h - n * ratio) * 0.5);
  }

  ratio = 2829 / 473;
  n = w * 0.8;
  currentBanner.width(n);
  currentBanner.height(n * ratio);
  currentBanner.x((w - n) * 0.5);
  currentBanner.y((h - n * ratio) * 0.5);
  currentBannerStroke.width(n);
  currentBannerStroke.height(n * ratio);
  currentBannerStroke.x((w - n) * 0.5);
  currentBannerStroke.y((h - n * ratio) * 0.5);

  currentPatternContainer.width(n);
  currentPatternContainer.height(n * ratio);
  currentPatternContainer.x((w - n) * 0.5);
  currentPatternContainer.y((h - n * ratio) * 0.5);
}

// ------------- utilities

function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;

  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function getHSLString(hue, saturation, lightness) {
  if (!saturation) saturation = settings.saturation;
  if (!lightness) lightness = settings.luminosity;

  return "hsl(" + Math.round(hue) + "," + saturation + "%, " + lightness + "%)";
}

function miscela() {
  settings.seed = (Math.round(Math.random() * 100) / 100).toString();
  Math.seedrandom(settings.seed);

  createLetters();
  changeLettersColours();
  updateSettingsHash();
  resize();
}

function esporta() {
  var currentScaleIndex = SCALES.indexOf(settings.scalePattern);

  // do some clean up!
  for (i in patternPaths) {
    if (patternPaths[i][currentScaleIndex].style("display") == "none") {
      patternPaths[i][currentScaleIndex].remove();
      console.log("removed");
    }
  }

  var svg = document.getElementById("SvgjsSvg1000");

  var serializer = new XMLSerializer();
  var source = serializer.serializeToString(svg);

  if (!source.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)) {
    source = source.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
  }
  if (!source.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)) {
    source = source.replace(
      /^<svg/,
      '<svg xmlns:xlink="http://www.w3.org/1999/xlink"'
    );
  }

  source = '<?xml version="1.0" standalone="no"?>\r\n' + source;

  var url = "data:image/svg+xml;charset=utf-8," + encodeURIComponent(source);

  window.open(url);
}
