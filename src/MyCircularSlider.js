import React from "react";
import CircularSlider from "@fseehawer/react-circular-slider";
export const MyCircularSlider = ({ value, onchangeHue }) => {
  const color = `hsl(${value}, 90%, 75%)`;

  return (
    <CircularSlider
      width="200"
      hideKnob="true"
      dataIndex={value}
      hideLabelValue="true"
      progressColorFrom={color}
      progressColorTo={color}
      trackColor="rgba(255,255,255,.2)"
      progressSize={30}
      trackSize={30}
      onChange={onchangeHue}
    />
  );
};
