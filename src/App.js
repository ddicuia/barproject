import React, { useState, useEffect } from "react";
import "./App.css";
import LogoGenerator, { BANNER_HEIGHT, BANNER_WIDTH } from "./LogoGenerator";
import { shuffle } from "./LogoGenerator";
import { createBrowserHistory } from "history";
import { GLASSES, BASES, SCALES, FLAVORS } from "./constants";
import { MyCircularSlider } from "./MyCircularSlider";
import { ToggleButton } from "./ToggleButton";
import { svgAsPngUri } from "save-svg-as-png";

const bgColors = [null, "#66FFDD", "#66C5FF", "#BD66FF", null, null];

const hashes = ["start", "bicchiere", "base", "gusto", "colore", "fine"];
const basiAnalcoliche = ["Nettari", "Soda", "Ghiaccio"];

let logoGenerator;
let finalCanvas;

function App() {
  const history = createBrowserHistory();
  const unlistenHistory = history.listen((location, action) => {
    const stageIndex = hashes.indexOf(location.hash.slice(1));
    if (state.stage !== stageIndex && stageIndex !== -1) {
      setState({ ...state, stage: stageIndex });
    }
  });

  const [state, setState] = useState({
    settings: {
      hue: Math.random() * 360,
      patterns: [],
      bases: [BASES[0]],
      glass: GLASSES[0],
      scalePattern: SCALES[SCALES.length - 1],
    },
  });

  const scale = window.innerWidth > 800 ? 1 : 0.5;
  let svgContainer;

  const salva = () => {
    const clone = svgContainer.cloneNode(true);

    clone.style.transform = "scale(1)";

    svgAsPngUri(clone, {
      width: BANNER_WIDTH,
      height: BANNER_HEIGHT,
    }).then((d) => {
      const ctx = finalCanvas.getContext("2d");
      const img = new Image();

      const w = BANNER_WIDTH;
      const h = BANNER_HEIGHT;
      console.log(window.devicePixelRatio);

      img.onload = () => {
        ctx.fillStyle = complementareB;
        ctx.fillRect(0, 0, 1080, 1080);
        ctx.drawImage(
          img,
          (1080 - BANNER_WIDTH) / 2,
          (1080 - BANNER_HEIGHT) / 2,
          w,
          h
        );
        ctx.fillStyle = "#333";
        ctx.font = "48px Benton";
        ctx.textAlign = "center";
        ctx.fillText("www.barproject.it", 1080 / 2, 1080 - 100);
        ctx.strokeStyle = "#333";
        ctx.lineWidth = 80;
        ctx.strokeRect(0, 0, 1080, 1080);
        //
        const uri = finalCanvas.toDataURL();
        const byteString = window.atob(uri.split(",")[1]);
        const mimeString = uri.split(",")[0].split(":")[1].split(";")[0];
        const buffer = new ArrayBuffer(byteString.length);
        const intArray = new Uint8Array(buffer);
        for (let i = 0; i < byteString.length; i++) {
          intArray[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([buffer], { type: mimeString });
        const saveLink = document.createElement("a");
        saveLink.download = "barprojcet";
        saveLink.style.display = "none";
        document.body.appendChild(saveLink);
        try {
          const url = URL.createObjectURL(blob);
          saveLink.href = url;
          saveLink.onclick = () =>
            setTimeout(() => URL.revokeObjectURL(url), 1000);
        } catch (e) {
          console.error(e);
          console.warn(
            "Error while getting object URL. Falling back to string URL."
          );
          saveLink.href = uri;
        }
        saveLink.click();
        document.body.removeChild(saveLink);
      };
      img.src = d;
    });
  };

  const onchangeHue = (hue) => {
    logoGenerator.changeHue(hue);
    state.settings.hue = hue;
    setState({ ...state, settings: state.settings });
  };

  useEffect(() => {
    if (!logoGenerator) {
      logoGenerator = new LogoGenerator(svgContainer);
      logoGenerator.init().then((d) => {
        logoGenerator.generate(state.settings);
        setState({
          ...state,
          logoGenerator,
        });
        history.push(`#${hashes[0]}`);
      });
    }

    return () => {
      unlistenHistory();
    };
  });

  const BasePage = () => {
    const getBaseButton = (d) => {
      return (
        <ToggleButton
          key={d}
          label={d}
          active={state.settings.bases.includes(d)}
          onActive={() => {
            state.settings.bases.push(d);
            if (state.settings.bases.length > 4) {
              state.settings.bases.splice(0, 1);
            }
            logoGenerator.changeLetters(state.settings.bases);
            setState({ ...state });
          }}
          onDeactive={() => {
            state.settings.bases.splice(state.settings.bases.indexOf(d), 1);
            logoGenerator.changeLetters(state.settings.bases);
            setState({ ...state });
          }}
        ></ToggleButton>
      );
    };

    return (
      <div className="baseDiv">
        <h2>Seleziona una base:</h2>

        <h3>Analcolica</h3>
        <div className="btnList">
          {BASES.filter((d) => basiAnalcoliche.includes(d)).map((d) =>
            getBaseButton(d)
          )}
        </div>

        <h3>Alcolica</h3>
        <div className="btnList">
          {BASES.filter((d) => basiAnalcoliche.indexOf(d) === -1).map((d) =>
            getBaseButton(d)
          )}
        </div>
      </div>
    );
  };

  const stages = [
    <h2 style={{ textAlign: "center" }}>
      Componi la ricetta del tuo drink preferito e realizza il tuo #cocktailogo
      personalizzato.
    </h2>,
    <div className="baseDiv">
      <h2>Scegli un bicchiere:</h2>
      <div>
        {GLASSES.map((d) => (
          <ToggleButton
            key={d}
            label={d}
            active={state.settings.glass === d}
            onActive={() => {
              state.settings.glass = d;
              logoGenerator.changeBanner(d, state.settings.hue);
              setState({ ...state });
            }}
            onDeactive={() => {}}
          ></ToggleButton>
        ))}
      </div>
    </div>,
    <BasePage />,
    <div className="baseDiv">
      <h2>Qual è il tuo gusto preferito?</h2>
      {FLAVORS.map((d) => (
        <ToggleButton
          key={d}
          label={d}
          active={state.settings.patterns.includes(d)}
          onActive={() => {
            state.settings.patterns.push(d);
            if (state.settings.patterns.length > 2) {
              state.settings.patterns.splice(0, 1);
            }
            logoGenerator.changePatterns(
              state.settings.patterns,
              state.settings.hue
            );
            setState({ ...state });
          }}
          onDeactive={() => {
            state.settings.patterns.splice(
              state.settings.patterns.indexOf(d),
              1
            );
            logoGenerator.changePatterns(
              state.settings.patterns,
              state.settings.hue
            );
            setState({ ...state });
          }}
        ></ToggleButton>
      ))}
    </div>,
    <div>
      <h2 style={{ color: "white", textAlign: "center", marginBottom: 40 }}>
        Scegli il tuo colore
      </h2>
      <MyCircularSlider
        value={state.settings.hue}
        label="colore"
        onchangeHue={onchangeHue}
      />
    </div>,
    <div style={{ color: "white", textAlign: "center" }}>
      <h1>Congratulazioni!</h1>
      <p>Scarica e condividi il tuo #cocktailogo personalizzato</p>
    </div>,
  ];

  const PrevNext = () => {
    const style = {
      background: "red",
    };
    return (
      <div className="prevNext">
        <button
          className="prevNextBtn"
          onClick={() => {
            history.push(`#${hashes[state.stage - 1]}`);
          }}
        >
          indietro
        </button>
        <button
          className="prevNextBtn"
          onClick={() => {
            history.push(`#${hashes[state.stage + 1]}`);
          }}
        >
          avanti
        </button>
      </div>
    );
  };

  const Inizia = (
    <button
      className="bigBtn inizia"
      onClick={() => {
        history.push(`#${hashes[1]}`);
      }}
    >
      INIZIA
    </button>
  );

  const Condividi = () => (
    <button className="bigBtn condividi" onClick={salva}>
      salva
    </button>
  );

  const Rimescola = () => (
    <button
      className="bigBtn rimescola"
      onClick={() => {
        const glass = shuffle([].concat(GLASSES))[0];
        const patterns = shuffle([].concat(FLAVORS)).slice(0, 2);
        const bases = shuffle([].concat(BASES)).slice(
          0,
          Math.round(Math.random() * 2) + 1
        );

        const newSettings = {
          bases,
          patterns,
          glass,
          hue: Math.random() * 360,
        };

        setState({
          ...state,
          settings: {
            ...state.settings,
            ...newSettings,
          },
        });
        logoGenerator.changeLetters(newSettings.bases);
        logoGenerator.changeBanner(newSettings.glass, newSettings.hue);
        logoGenerator.changePatterns(newSettings.patterns, newSettings.hue);
      }}
    >
      shekera
    </button>
  );

  const getNav = () => {
    switch (state.stage) {
      case 0:
        return <div className="navigation">{Inizia}</div>;
      default:
        return (
          <div className="navigation">
            <PrevNext></PrevNext>
          </div>
        );
    }
  };

  const getFinalBtns = () => {
    return (
      <div className="finalBtns">
        <Condividi />
        <Rimescola />
      </div>
    );
  };

  const hueString = (state.settings.hue + 100) % 360;
  const complementareB = `hsl(${(hueString + 50) % 360}, 80%, 65%)`;
  const backgroundColor =
    state.stage === stages.length - 1 || state.stage === 0 || state.stage === 4
      ? complementareB
      : bgColors[state.stage];

  return (
    <div
      className="App"
      style={{
        background: backgroundColor,
      }}
    >
      <canvas
        width="1080"
        height="1080"
        style={{
          top: 0,
          left: 0,
          position: "absolute",
          display: "none",
        }}
        ref={(el) => (finalCanvas = el)}
      ></canvas>

      <div
        id="loading"
        style={{ visibility: !logoGenerator ? "visible" : "hidden" }}
      >
        <div className="spinner">
          <div className="mask">
            <div className="maskedCircle"></div>
          </div>
        </div>
      </div>

      <div
        className="logo"
        style={{
          transform: `translate(-50%,0 ) scale(${scale}) `,
        }}
      >
        <svg
          ref={(el) => (svgContainer = el)}
          id="drawing"
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
        ></svg>
      </div>

      <div
        className="wrapper"
        style={{ visibility: logoGenerator ? "visible" : "hidden" }}
      >
        <div className="container">
          <main
            style={{
              marginTop: scale < 1 ? 150 : 200,
            }}
          >
            {stages[state.stage]}
          </main>
        </div>
      </div>

      <footer style={{ visibility: logoGenerator ? "visible" : "hidden" }}>
        {state.stage < stages.length - 1 ? getNav() : getFinalBtns()}
      </footer>
    </div>
  );
}

export default App;
