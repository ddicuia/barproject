import React from "react";
export const ToggleButton = ({ active, label, onActive, onDeactive }) => {
  return (
    <div
      key={label}
      className="textBtn"
      onClick={() => {
        active ? onDeactive() : onActive();
      }}
    >
      <span className={active ? "active" : null}>{label}</span>
    </div>
  );
};
