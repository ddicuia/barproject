export const STRING = "B A R P R1 O J E C T".split(" ");
export const BASES = [
  "Ghiaccio",
  "Nettari",
  "Soda",
  "Brandy",
  "Cachaca",
  "Gin",
  "Grappa",
  "Mezcal",
  "Pisco",
  "Sake",
  "Rum",
  "Tequila",
  "Vodka",
  "Whysky",
];

export const FLAVORS = [
  "speziato",
  "erbaceo",
  "amaro",
  "dolce",
  "agrumato",
  "fruttato",
  "alcolico",
  "aspro",
  "fresco",
];
export const GLASSES = ["shot", "rock", "cocktail", "wine", "flute", "tumbler"];

export const SCALES = ["piccola", "media", "grande"];
