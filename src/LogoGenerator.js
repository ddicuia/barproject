import { SVG } from "@svgdotjs/svg.js";
import { STRING } from "./constants";
import loadSVG from "./SVGLoader";

let stage;
let currentBanner;
let currentPatternContainer;
let currentBannerStroke;
let currentBannerPath;

const CONTAINER_WIDTH = 2829;
const CONTAINER_HEIGHT = 473;
const PATTERN_WIDTH = 322.561;
const PATTERN_HEIGHT = 315.578;
const LETTER_RATIO = 364.688 / 347.386;
const CONTAINER_RATIO = CONTAINER_WIDTH / CONTAINER_HEIGHT;
export const BANNER_WIDTH = 800;
export const BANNER_HEIGHT = 200;

var currentPatterns = [];
var currentLetters = [];

let w = BANNER_WIDTH;
let h = BANNER_HEIGHT;

export default class LogoGenerator {
  constructor(svgEL) {
    stage = SVG(svgEL);
    stage.css("width", w + "px");
    stage.size(w, h);
  }

  init = (d) => {
    return new Promise(async (resolve) => {
      const { letterPaths, bannerPaths, patterns } = await loadSVG();
      this.letterPaths = letterPaths;
      this.bannerPaths = bannerPaths;
      this.patternContainers = {};
      this.flavourSettings = {};
      // instantiate containers
      currentBanner = stage.nested();
      currentPatternContainer = stage.nested();
      currentBannerStroke = stage.nested();
      currentBannerStroke.attr("id", "wtf");

      // instantiate viewboxes
      currentBanner.viewbox({
        x: 0,
        y: 0,
        width: CONTAINER_WIDTH,
        height: CONTAINER_HEIGHT,
      });
      currentBannerStroke.viewbox({
        x: 0,
        y: 0,
        width: CONTAINER_WIDTH,
        height: CONTAINER_HEIGHT,
      });
      currentPatternContainer.viewbox({
        x: 0,
        y: 0,
        width: CONTAINER_WIDTH,
        height: CONTAINER_HEIGHT,
      });

      let scale = 1;

      patterns.forEach((pattern) => {
        let id = pattern.attr("id");

        let patternContainer = currentPatternContainer.nested();
        let patternPath = SVG(pattern.node);
        patternPath.children().forEach((a) => {
          a.children().forEach((b) => {
            if (b.attr("stroke") === "#FF0000") {
              b.remove();
            } else {
              b.attr("stroke-width", 10);
            }
          });
        });

        const bb = patternPath.bbox();

        let w = bb.width;
        let h = bb.height;

        // create the actual pattern
        for (let x = 0; x < 100; x++) {
          for (let y = -1; y < 50; y++) {
            const scaledX = (x * w - w * 0.5) * scale;
            const scaledY = (y * h - h * 0.5) * scale - h * 0.5 * scale;
            if (scaledX > CONTAINER_WIDTH) break;
            if (scaledY > CONTAINER_HEIGHT) break;
            let nested = patternContainer.nested();
            patternPath.clone().addTo(nested);
            nested.viewbox({
              x: 0,
              y: 0,
              width: PATTERN_WIDTH,
              height: PATTERN_HEIGHT,
            });
            nested.width(PATTERN_WIDTH * scale);
            nested.height(PATTERN_HEIGHT * scale);
            nested.x(scaledX);
            nested.y(scaledY);
          }
        }
        this.patternContainers[id] = patternContainer;
        patternContainer.css("display", "none");
      });

      resolve();
    });
  };

  generate(settings) {
    console.log("generating");

    this.changePatterns(settings.patterns, settings.hue);
    this.changeLetters(settings.bases);
    this.changeBanner(settings.glass, settings.hue);
    this.resize();
  }

  changePatterns(patterns, hue) {
    currentPatterns = [];

    Object.keys(this.patternContainers).forEach((pattern) => {
      const isActive = patterns.includes(pattern);
      const patternG = this.patternContainers[pattern];
      patternG.css("display", isActive ? "block" : "none");
      if (isActive) currentPatterns.push(patternG);
    });

    this.updatePatternsColors(hue);
  }

  updatePatternsColors(hue) {
    const hueString = (hue + 100) % 360;
    const complementare = `hsl(${hueString}, 90%, 75%)`;
    const complementareB = `hsl(${(hueString + 50) % 360}, 90%, 75%)`;
    const availableStrokes = [complementare, complementareB];

    currentPatterns.forEach((patternPath, i) => {
      patternPath
        .children()
        .first()
        .children()
        .forEach((a) => {
          a.children().forEach((b) => {
            b.stroke(availableStrokes[i]);
          });
        });
    });
  }

  changeLetters(bases) {
    // assign bases to letters
    let a = [];
    for (let i = 0; i < STRING.length; i++) {
      a.push(i % bases.length);
    }
    shuffle(a);
    //remove current letters
    for (let i = 0; i < currentLetters.length; i++) {
      currentLetters[i].remove();
    }
    currentLetters = [];
    for (let i = 0; i < STRING.length; i++) {
      let letterName = STRING[i];
      let p = this.letterPaths[letterName][bases[a[i]]].path;
      let group = stage.nested();
      group.svg(p.node.innerHTML);
      group.viewbox({
        x: 0,
        y: 0,
        width: 347.386,
        height: 364.688,
      });
      currentLetters.push(group);
    }
    this.resize();
  }

  changeBanner(glass, hue) {
    currentBanner.clear();
    currentBanner.back();
    currentBannerStroke.clear();

    const p = this.bannerPaths[glass].node.innerHTML;
    currentBannerStroke.svg(p);

    // console.log(currentBannerStroke);

    let isPolygon = false;
    let path = currentBannerStroke.node.querySelector("path");

    if (!path) {
      path = currentBannerStroke.node.querySelector("polygon");
      isPolygon = true;
    }

    if (!path) {
      throw new Error("Can't find the banner to clip");
    }

    currentBannerPath = SVG(path).clone();
    currentBanner.add(currentBannerPath);

    const clip = isPolygon
      ? currentPatternContainer.polygon(currentBannerPath.attr("points"))
      : currentPatternContainer.path(currentBannerPath.attr("d"));

    currentPatternContainer.clipWith(clip.fill("#000"));

    this.changeHue(hue);
  }

  changeHue(hue) {
    currentBannerPath.attr("fill", `hsl(${hue}, 90%, 75%)`);
    this.updatePatternsColors(hue);
  }

  resize() {
    var padding = w * 0.1;
    var z = w - padding * 2;
    var k = z / STRING.length;
    const letterWidth = k * 0.8;
    const letterHeight = letterWidth * LETTER_RATIO;

    var startX = (w - z) * 0.5;

    // position letters
    for (var i = 0; i < STRING.length; i++) {
      var group = currentLetters[i];
      group.width(letterWidth);
      group.height(letterHeight);
      group.x(i * k + startX);
      group.y((h - letterHeight) * 0.5);
    }

    const n = w;
    currentBanner.width(n);
    currentBanner.height(n * CONTAINER_RATIO);
    currentBanner.x((w - n) * 0.5);
    currentBanner.y((h - n * CONTAINER_RATIO) * 0.5);
    currentBannerStroke.width(n);
    currentBannerStroke.height(n * CONTAINER_RATIO);
    currentBannerStroke.x((w - n) * 0.5);
    currentBannerStroke.y((h - n * CONTAINER_RATIO) * 0.5);

    currentPatternContainer.width(n);
    currentPatternContainer.height(n * CONTAINER_RATIO);
    currentPatternContainer.x((w - n) * 0.5);
    currentPatternContainer.y((h - n * CONTAINER_RATIO) * 0.5);
  }
}

export const shuffle = (array) => {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
};
