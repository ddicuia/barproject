import { SVG } from "@svgdotjs/svg.js";
import { STRING, BASES } from "./constants";

export default () => {
	return new Promise((resolve) => {
		const letterPaths = {};
		const bannerPaths = {};
		let patterns = {};

		load("letters.svg", onLettersLoaded)
			.then((d) => {
				return load("banners.svg", onBannersLoaded);
			})
			.then((d) => {
				return load("patterns.svg", onPatternsLoaded);
			})
			.then(() => {
				resolve({ letterPaths, bannerPaths, patterns });
			});

		function load(assetsURL, callback) {
			return new Promise((resolve) => {
				fetch("assets/" + assetsURL)
					.then((r) => r.text())
					.then(callback)
					.then(resolve);
			});
		}

		// ------------- loading callbacks

		function onLettersLoaded(a) {
			return new Promise((resolve) => {
				const n = document.createElement("div");
				n.innerHTML = a;

				var letters = SVG(n).find("svg > g");
				for (var i = 0; i < letters.length; i++) {
					addToList(letters[i]);
				}

				function addToList($el) {
					// trim crap added by illustrator
					var name = $el.attr("id");
					var regexpatterns = ["__x2013__", "_-_"];
					var found = false;

					for (var i = 0; i < regexpatterns.length; i++) {
						var pattern = regexpatterns[i];

						if (name.match(pattern)) {
							var a = name.split(pattern);
							var letterName = a[0];
							var baseName = a[1];

							if (baseName.match("_1_")) {
								baseName = baseName.split("_1_")[0];
							}

							// check for errors
							if (STRING.indexOf(letterName) == -1)
								console.error("Layer ", name, " is not named properly");
							if (BASES.indexOf(baseName) == -1)
								console.error("Layer ", name, " is not named properly");

							if (!letterPaths[letterName]) {
								letterPaths[letterName] = {};
							}

							if (!letterPaths[letterName][baseName]) {
								letterPaths[letterName][baseName] = {};
							}
							letterPaths[letterName][baseName].path = $el;
							found = true;
							break;
						}
					}

					if (!found)
						console.error("Couldn't find letter - has the svg changed?");
				}

				resolve();
			});
		}

		function onBannersLoaded(a) {
			return new Promise((resolve) => {
				const n = document.createElement("div");
				n.innerHTML = a;

				var banners = SVG(n).find("svg > g");

				for (var i = 0; i < banners.length; i++) {
					var id = banners[i].attr("id");
					bannerPaths[id] = banners[i];
				}
				resolve();
			});
		}

		function onPatternsLoaded(a) {
			const n = document.createElement("div");
			return new Promise((resolve) => {
				n.innerHTML = a;
				patterns = SVG(n).find("svg > g");
				resolve();
			});
		}
	});
};
